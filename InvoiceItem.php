<?php
namespace dpavic\assignment1;

class InvoiceItem
{
	private $product;
	private $productAmount;
	private $itemPrice;
	private $totalPrice;


	function __construct($product, $itemPrice, $productAmount)
	{
		$this->product = $product;
		$this->itemPrice = $itemPrice;
		$this->productAmount = $productAmount;
		$this->totalPrice = $itemPrice * $productAmount;
	}


	/**
	 * @param mixed $product
	 */
	public function setProduct(Product $product)
	{
		$this->product = $product;
	}

	/**
	 * @return Product
	 */
	public function getProduct()
	{
		return $this->product;
	}

	/**
	 * @param mixed $itemPrice
	 */
	public function setItemPrice($itemPrice)
	{
		$this->itemPrice = $itemPrice;
	}

	/**
	 * @return mixed
	 */
	public function getItemPrice()
	{
		return $this->itemPrice;
	}

	/**
	 * @param mixed $productAmount
	 */
	public function setProductAmount($productAmount)
	{
		$this->productAmount = $productAmount;
	}

	/**
	 * @return mixed
	 */
	public function getProductAmount()
	{
		return $this->productAmount;
	}


	/**
	 * @return mixed
	 */
	public function getTotalPrice()
	{
		return $this->totalPrice;
	}




}