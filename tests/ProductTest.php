<?php

use dpavic\assignment1\Product;

require_once __DIR__ . '/../Product.php';

class ProductTest extends PHPUnit_Framework_TestCase
{

	public function testGetName()
	{
		$product = new Product("Proizvod A", 1);
		$this->assertSame("Proizvod A", $product->getName());
		$product->setName("Proizvod B");
		$this->assertSame("Proizvod B", $product->getName());
	}

	public function testGetCategory()
	{
		$product = new Product("Proizvod A", 1);
		$this->assertSame('Laptop', $product->getCategory());
		$product->setCategory(2);
		$this->assertSame('Monitor', $product->getCategory());
	}

}