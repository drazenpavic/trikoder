<?php

use dpavic\assignment1\InvoiceItem;
use dpavic\assignment1\Product;

require_once __DIR__ . '/../InvoiceItem.php';

class InvoiceItemTest extends PHPUnit_Framework_TestCase
{

	public function testGetProduct()
	{
		$product = new Product("Proizvod A", 1);
		$invoiceItem = new InvoiceItem($product, 100, 2);
		$this->assertSame($product, $invoiceItem->getProduct());
	}

	public function testGetItemPrice()
	{
		$product = new Product("Proizvod A", 1);
		$invoiceItem = new InvoiceItem($product, 100, 2);
		$this->assertSame(100, $invoiceItem->getItemPrice());
	}

	public function testGetProductAmount()
	{
		$product = new Product("Proizvod A", 1);
		$invoiceItem = new InvoiceItem($product, 100, 2);
		$this->assertSame(2, $invoiceItem->getProductAmount());
	}

	public function testGetTotalPrice()
	{
		$product = new Product("Proizvod A", 1);
		$invoiceItem = new InvoiceItem($product, 100, 2);
		$this->assertSame(200, $invoiceItem->getTotalPrice());
	}
}