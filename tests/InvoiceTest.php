<?php

use dpavic\assignment1\InvoiceItem;
use dpavic\assignment1\Product;

require_once __DIR__ . '/../Invoice.php';

class InvoiceTest extends PHPUnit_Framework_TestCase
{

	public function testGetInvoiceNumber()
	{
		$product = new Product("Proizvod A", 1);
		$invoiceItem = new InvoiceItem($product, 100, 2);
		$customer = 'Drazen Pavic, 098/783354';
		$invoice = new \dpavic\assignment1\Invoice(1, '2014-08-01', $customer);
		$this->assertSame(1, $invoice->getInvoiceNumber());
	}

	public function testGetCreationDate()
	{
		$product = new Product("Proizvod A", 1);
		$invoiceItem = new InvoiceItem($product, 100, 2);
		$customer = 'Drazen Pavic, 098/783354';
		$invoice = new \dpavic\assignment1\Invoice(1, '2014-08-01', $customer);
		$date = date('Y-m-d');
		$this->assertSame($date, $invoice->getCreationDate());
	}

	public function testGetDueDate()
	{
		$product = new Product("Proizvod A", 1);
		$invoiceItem = new InvoiceItem($product, 100, 2);
		$customer = 'Drazen Pavic, 098/783354';
		$invoice = new \dpavic\assignment1\Invoice(1, '2014-08-01', $customer);
		$this->assertSame('2014-08-01', $invoice->getDueDate());
	}

	public function testAddItem()
	{
		$product = new Product("Proizvod A", 1);
		$invoiceItem = new InvoiceItem($product, 100, 2);
		$customer = 'Drazen Pavic, 098/783354';
		$invoice = new \dpavic\assignment1\Invoice(1, '2014-08-01', $customer);
		$invoice->addItem($invoiceItem);
		$this->assertSame([0 => $invoiceItem], $invoice->getInvoiceItems());
	}

	public function testGetNettoAmount()
	{
		$product = new Product("Proizvod A", 1);
		$invoiceItem = new InvoiceItem($product, 100, 2);
		$customer = 'Drazen Pavic, 098/783354';
		$invoice = new \dpavic\assignment1\Invoice(1, '2014-08-01', $customer);
		$invoice->addItem($invoiceItem);
		$this->assertSame(200, $invoice->getNettoAmount());
	}

	public function testGetBruttoAmount()
	{
		$product = new Product("Proizvod A", 1);
		$invoiceItem = new InvoiceItem($product, 100, 2);
		$customer = 'Drazen Pavic, 098/783354';
		$invoice = new \dpavic\assignment1\Invoice(1, '2014-08-01', $customer);
		$invoice->addItem($invoiceItem);
		$this->assertSame(100*2*1.25, $invoice->getBruttoAmount());
	}

	public function testGetTaxTotal()
	{
		$product = new Product("Proizvod A", 1);
		$invoiceItem = new InvoiceItem($product, 100, 2);
		$customer = 'Drazen Pavic, 098/783354';
		$invoice = new \dpavic\assignment1\Invoice(1, '2014-08-01', $customer);
		$invoice->addItem($invoiceItem);
		$this->assertSame(100*2*1.25-200, $invoice->getTaxTotal());
	}

}