<?php
namespace dpavic\assignment1;

class Invoice
{
	private $invoiceNumber;
	private $creationDate;
	private $dueDate;
	private $customer;
	private $items = array();


	function __construct($invoiceNumber, $dueDate, $customer)
	{
		$this->invoiceNumber = $invoiceNumber;
		$this->dueDate = $dueDate;
		$this->customer = $customer;
		$this->creationDate = date('Y-m-d');
	}


	/**
	 * @param mixed $invoiceNumber
	 */
	public function setInvoiceNumber($invoiceNumber)
	{
		$this->invoiceNumber = $invoiceNumber;
	}

	/**
	 * @return mixed
	 */
	public function getInvoiceNumber()
	{
		return $this->invoiceNumber;
	}

	/**
	 * @param mixed $creationDate
	 */
	public function setCreationDate($creationDate)
	{
		$this->creationDate = $creationDate;
	}

	/**
	 * @return mixed
	 */
	public function getCreationDate()
	{
		return $this->creationDate;
	}

	/**
	 * @param mixed $dueDate
	 */
	public function setDueDate($dueDate)
	{
		$this->dueDate = $dueDate;
	}

	/**
	 * @return mixed
	 */
	public function getDueDate()
	{
		return $this->dueDate;
	}

	/**
	 * @param InvoiceItem $invoiceItem
	 */
	public function addItem(InvoiceItem $invoiceItem)
	{
		$this->items[] = $invoiceItem;
	}


	/**
	 * @return array
	 */
	public function getInvoiceItems()
	{
		return $this->items;
	}

	/**
	 * @return int|mixed
	 */
	public function getNettoAmount()
	{
		$total = 0;

		/* @var $item InvoiceItem */
		foreach ($this->items as $item) {
			$total += $item->getTotalPrice();
		}

		return $total;
	}

	/**
	 * @return int|mixed
	 */
	public function getBruttoAmount()
	{
		return $this->getNettoAmount() * 1.25;
	}

	/**
	 * @return int|mixed
	 */
	public function getTaxTotal()
	{
		return $this->getBruttoAmount() - $this->getNettoAmount();
	}

}