<?php
namespace dpavic\assignment1;

class Product
{
	private $name;
	private $category;
	private static $categoryList = array(
		1 => 'Laptop',
		2 => 'Monitor',
		3 => 'Mouse'
	);

	function __construct($name, $category)
	{
		$this->setName($name);
		$this->setCategory($category);
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}


	/**
	 * @param mixed $category
	 */
	public function setCategory($category)
	{
		try {
			if (!isset (self::$categoryList[$category])) {
				throw new Exception('Unknown category');
			}
			$this->category = self::$categoryList[$category];
		} catch (Exception $e) {
			echo 'Caught exception: ' . $e->getMessage() . '<br />';

		}
	}

	/**
	 * @return mixed
	 */
	public function getCategory()
	{
		return $this->category;
	}


}
 